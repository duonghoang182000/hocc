#include <stdio.h>

void bubbleSort(int a[], int soPhanTu){
	for (int i = 0; i < soPhanTu - 1; i++){
		for (int j = soPhanTu - 1; j > i; j--){
			if(a[j] < a[j - 1]){
				int b = a[j];
				a[j] = a[j - 1];
				a[j - 1] = b;
			} 
		}  
	}
}

int* bubbleSort2(int a[], int soPhanTu){
	for (int i = 0; i < soPhanTu - 1; i++){
		for (int j = soPhanTu - 1; j > i; j--){
			if(a[j] < a[j - 1]){
				int b = a[j];
				a[j] = a[j - 1];
				a[j - 1] = b;
			} 
		}  
	}
	return a;
}

int main(){
	int array[] = {1, 5, 6, 7, 8, 2, 4, 30, 9};
	
	size_t e = sizeof(array) / sizeof(array[0]);
	int* a;
	bubbleSort(array, e);
	a = bubbleSort2(array, e);
	for(int i = 0; i < e; i++){  
        printf("%d\n", *(a + i));  
    }  
}
